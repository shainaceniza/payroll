import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from '@/components/Dashboard'
import Records from '@/components/Records'
import Calendar from '@/components/Calendar'


Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/records',
      name: 'Records',
      component: Records
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: Calendar
    }
    
  ]
})
